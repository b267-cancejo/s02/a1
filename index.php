
<?php require_once "./code.php"; ?>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S02: Activity</title>
	</head>
	<body>
		<h3>Print the Number Divisible of Five</h3>
		<p><?php printDivisibleOfFive(); ?></p>

		<?php array_push($students, "John Smith"); ?>
		<pre><?php print_r($students); ?></pre>
		<pre><?php echo count($students); ?></pre>

		<?php array_push($students, "Jane Smith"); ?>
		<pre><?php print_r($students); ?></pre>
		<pre><?php echo count($students); ?></pre>

		<?php array_shift($students); ?>
		<pre><?php print_r($students); ?></pre>
		<pre><?php echo count($students); ?></pre>

	</body>
</html>